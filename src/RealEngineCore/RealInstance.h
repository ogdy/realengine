#pragma once

// Internal
#include "RealObject.h"
#include "Mesh.h"

// External
//#pragma managed(push, off)
#include "CoreRealInstance.h"
//#pragma managed(pop)

using namespace System;

public ref class RealInstance {
public:
	RealInstance();
	void Run();
	void Add(Mesh^ realObject);
private:
	CoreRealInstance* coreRealInstance;
};