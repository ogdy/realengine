#pragma once

// External
#include "CoreMesh.h"

// Internal
using namespace System;

public ref class Mesh {
public:
	Mesh();
	void SetIndices(int* pos, int size);
	void SetVertices(float* pos, int size);
	void SetUVs(float* pos, int size);
	CoreMesh* coreMesh;
private:
	
};