#pragma once

// External
//#include "CoreRealInstance.cpp"

// Internal
#include "RealInstance.h"

RealInstance::RealInstance() {
	coreRealInstance = new CoreRealInstance();
}

void RealInstance::Run() {
	coreRealInstance->Run();
}

void RealInstance::Add(Mesh^ realObject) {
	coreRealInstance->Add(realObject->coreMesh);
}