#pragma once

#include <vector>
#include "Vertex.h"

using namespace std;

class CoreMesh {
public:
	CoreMesh();
	void SetIndices(int* pos, int size);
	void SetVertices(float* pos, int size);
	void SetUVs(float* pos, int size);
	std::vector<Vertex> Vertices;
	std::vector<uint32_t> Indices;
private:

};