#include "main.h"

// Only going there if compiled as Exe
int main() {
	
	CoreRealInstance app;

	try {
	app.Run();
	}
	catch (const std::runtime_error& e) {
	std::cerr << e.what() << std::endl;
	return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;

	return 0;
}