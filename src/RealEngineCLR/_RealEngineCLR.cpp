#pragma once

#include "stdafx.h"

// Header
#include "RealEngineCLR.h"

//#include <comutil.h>
#include <string>  
#include <iostream> 
using namespace System;
using namespace std;

public delegate void SomethingHappenedEventHandler();
public delegate void MessageEventHandler(const char *str);

__declspec(dllexport) void Awake() {
	OGXD::RealEngine::RealEngineCore::InvokeAwaked();
}

__declspec(dllexport) void Start() {
	OGXD::RealEngine::RealEngineCore::InvokeStarted();
}

__declspec(dllexport) void Update() {
	OGXD::RealEngine::RealEngineCore::InvokeUpdated();
}

__declspec(dllexport) void Message(string message) {
	String^ MyString = gcnew String(message.c_str());
	OGXD::RealEngine::RealEngineCore::InvokeMessageReceived(MyString);
}