#pragma once

#include "stdafx.h"
#include "Mesh.h"
#include "..\RealEngineCore\CoreMesh.cpp"

Mesh::Mesh(Defacto::Graphics::Mesh::Mesh mesh) {
	coreMesh = new CoreMesh();
	Defacto::Graphics::Mesh::UnsafeMesh^ unsafeMesh = mesh.getUnsafe();
	coreMesh->SetIndices(unsafeMesh->trianglesPosition, unsafeMesh->trianglesSize);
	coreMesh->SetVertices(unsafeMesh->verticesPosition, unsafeMesh->verticesSize);
}