﻿using System;
using CSMESH = Defacto.Graphics.Mesh;
 
namespace OGXD.RealEngine.Samples {
    class Program {
        delegate double MyCallback(double x, double y);

        //[DllImport(@"RealEngineCore.dll", CallingConvention = CallingConvention.Cdecl)]
        //private extern static int run();

        static void Main(string[] args) {

            CSMESH.Mesh csMesh = CSMESH.Loaders.Load<CSMESH.Mesh>(@"models\suzanne.obj");
            CSMESH.UnsafeMesh unsafeMesh = csMesh.getUnsafe();
            Mesh mesh = new Mesh();

            unsafe
            {
                mesh.SetIndices(unsafeMesh.trianglesPosition, unsafeMesh.trianglesSize);
                mesh.SetVertices(unsafeMesh.verticesPosition, unsafeMesh.verticesSize);
            }

            RealInstance realInstance = new RealInstance();
            realInstance.Add(mesh);
            realInstance.Run();

            Console.WriteLine("Finished!");

            Console.ReadKey();
        }
    }

    //class TestScript : RealScript {
    //    public override void update() {
    //        Console.WriteLine("update !");
    //    }
    //}
}
