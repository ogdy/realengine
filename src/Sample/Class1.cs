﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample {
    class WeirdClass {

        /// Params argument (array)
        public WeirdClass(params dynamic[] weirdArgs) {

            /// One-line multiple values initializer
            int a, b, c = b = a = 10; 

            /// Quick block comments
            //*/ // First way
            a = b - 1;
            /*/ // Second way
            c = b - 1;
            //*/

            /// One-line if - assign
            b = (a < 10) ? c : a;

            /// Goto
            again:
            b++;
            c += a - b;
            if (b < 20)
                goto again;

            /// Coalescence null https://docs.microsoft.com/en-us/dotnet/csharp/language-reference/operators/null-conditional-operator
            int? d = null;
            a = d ?? -1;

            /// Other https://stackoverflow.com/questions/9033/hidden-features-of-c
            var aRef = __makeref(a);
            __refvalue(aRef, int) = 10;

            /// Hard string format
            string str = @"this is an annoying string \o/";

            /// Unicode names
            int ÌÜþ7WB = 5;
            a += ÌÜþ7WB;

            /*/ // First way
            a = b - 1;
            /*/ // Second way
            c = b - 1;
            //*/
        }

        [return: AttributeClass]
        int method() { return 0; }
    }

    public class AttributeClass : Attribute {

    }
}
