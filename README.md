# What is Real Engine ?

Real Engine is a personnal training project for GPU programming and for the use of the Vulkan API.
The goal is to achieve the highest degree of realism I can in full realtime.
The engine is and will probably not industry ready as it's an amateur project.

# Why another engine ?

This is mainly a training purpose, and I wanted to start from the very begining (from the first line of code). I don't really care about other projects, except for learning.

# Why the name "Real Engine" ?

The main reasons for that name are :
* An obvious lack of originality
* A bad sense of humor
* An excess of ambition due to the fact that I haven't began when I wrote this readme and I clearly underestimate the challenge.